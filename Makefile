#################################################################
# General settings       										#
#################################################################

templateFolder 			= /home/open-foam-cli/Documents/Templates/
openFoamSetupFolder 	= /home/open-foam-cli/Documents/Templates/openFoamSetup
openFoamScriptFolder 	= /home/open-foam-cli/Documents/Templates/openFoamScript

case_dir 		= $(shell pwd)

#################################################################
# Global case setups         									#
#################################################################

globalSetup:
	mkdir openFoam
	mkdir openFoam/case01
	mkdir openFoam/case02
	mkdir ansys
	mkdir ansys/case01
	mkdir ansys/case02		
	mkdir dakota
	mkdir dakota/case01
	mkdir dakota/case02
	mkdir mesh
	mkdir mesh/cad
	mkdir mesh/cad/native
	mkdir mesh/cad/stl 
	mkdir mesh/cad/vtk
	mkdir mesh/exchange
	mkdir docs
	mkdir docs/globalReports
	mkdir docs/literature
	mkdir docs/notes
	mkdir docs/calculations

openFoamGlobalSetup:
	mkdir openFoam
	mkdir openFoam/case01
	cp "$(openFoamSetupFolder)/Makefile" "./openFoam/case01/"
	mkdir mesh
	mkdir mesh/cad
	mkdir mesh/cad/native
	mkdir mesh/cad/stl 
	mkdir mesh/cad/vtk
	mkdir docs
	mkdir docs/globalReports
	mkdir docs/literature
	mkdir docs/notes
	mkdir docs/calculations
	mkdir tools 
	cp "$(openFoamSetupFolder)/Makefile" "./tools/"
	cp "$(openFoamSetupFolder)/.gitignore.global" "./.gitignore"
	cp "$(openFoamSetupFolder)/READMEglobal.md" "./README.md"
	git init
	git submodule add git@gitlab.probat.com:OpenFoam/Templates/openFoamScripte.git tools/openFoamScripte

#################################################################
# OpenFoam case setups         									#
#################################################################

simpleFoam: basicFileStruct meshFiles simpleFoamFiles incompFiles

pimpleFoam: basicFileStruct meshFiles pimpleFoamFiles incompFiles

rhoSimpleFoam: basicFileStruct meshFiles simpleFoamFiles compFiles

rhoPimpleFoam: basicFileStruct meshFiles pimpleFoamFiles compFiles

#################################################################
# Dependent Case Calls    										#
#################################################################

basicFileStruct:
	mkdir 0
	mkdir constant
	mkdir constant/triSurface			
	mkdir system
	mkdir docs
	mkdir docs/caseReport
	mkdir docs/literature
	mkdir docs/notes
	mkdir docs/calculations

meshFiles:	
	cp "$(openFoamSetupFolder)/system/blockMeshDict" "./system"
	cp "$(openFoamSetupFolder)/system/snappyHexMeshDict" "./system"
	cp "$(openFoamSetupFolder)/system/meshQualityDict" "./system"
	ln -s ../../mesh/cad/ cad

simpleFoamFiles:
	cp "$(openFoamSetupFolder)/0/U" "./0"
	cp "$(openFoamSetupFolder)/system/simpleFoam/controlDict" "./system"
	cp "$(openFoamSetupFolder)/system/simpleFoam/fvSchemes" "./system"
	cp "$(openFoamSetupFolder)/system/simpleFoam/fvSolution" "./system"

pimpleFoamFiles:
	cp "$(openFoamSetupFolder)/0/U" "./0"
	cp "$(openFoamSetupFolder)/system/pimpleFoam/controlDict" "./system"
	cp "$(openFoamSetupFolder)/system/pimpleFoam/fvSchemes" "./system"
	cp "$(openFoamSetupFolder)/system/pimpleFoam/fvSolution" "./system"

incompFiles:
	cp "$(openFoamSetupFolder)/0/p.incomp" "./0/p"
	cp "$(openFoamSetupFolder)/constant/transportProperties" "./constant"

compFiles:
	cp "$(templateFolder)/0/p.comp" "./0/p"
	cp "$(templateFolder)/constant/thermophysicalProperties" "./constant"

#################################################################
# Turbulence options      										#
#################################################################

kOmegaSST:
	cp "$(openFoamSetupFolder)/constant/turbulence/turbulenceProperties.kOmegaSST" "./constant/turbulenceProperties" 
	cp "$(openFoamSetupFolder)/0/k" "./0" 
	cp "$(openFoamSetupFolder)/0/omega" "./0" 

kOmega:
	cp "$(openFoamSetupFolder)/constant/turbulence/turbulenceProperties.kOmega" "./constant/turbulenceProperties" 
	cp "$(openFoamSetupFolder)/0/k" "./0" 
	cp "$(openFoamSetupFolder)/0/omega" "./0"

kEpsilon:
	cp "$(openFoamSetupFolder)/constant/turbulence/turbulenceProperties.kEpsilon" "./constant/turbulenceProperties" 
	cp "$(openFoamSetupFolder)/0/k" "./0" 
	cp "$(openFoamSetupFolder)/0/epsilon" "./0" 

#################################################################
# Dynamic mesh options 		     								#
#################################################################

dynamicMesh:	
	cp "$(openFoamSetupFolder)/constant/dynamicMeshDict" "./constant"

#################################################################
# Porosity options	 		     								#
#################################################################

porosity:	
	cp "$(openFoamSetupFolder)/constant/porousZone" "./constant"

#################################################################
# Git options 		     										#
#################################################################

gitOpenFoam:
	cp "$(openFoamSetupFolder)/READMEcase.md" "./README.md"
	cp "$(openFoamSetupFolder)/.gitignore.case" "./.gitignore"
	git init
	git add .
	git commit -m "Initial setup"