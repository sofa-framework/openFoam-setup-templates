# \CASENAME

![Avatar](/docs/Avatar.png)

-----------------------------------------

## Table of Contents

- [\CASENAME](#casename)
  - [Table of Contents](#table-of-contents)
  - [Project](#project)
    - [Project details](#project-details)
    - [Objective](#objective)
    - [Task list](#task-list)
    - [Issues](#issues)
  - [System](#system)
    - [Hardware](#hardware)
    - [Software](#software)
  - [CFD Setup](#cfd-setup)
    - [Analysis](#analysis)
    - [Fluid properties](#fluid-properties)
    - [Boundary conditions](#boundary-conditions)
    - [Usage](#usage)

## Project

### Project details

|            |     |
| ---------- | --- |
| Contakt    |     |
| Department |     |
| Start      |     |
| End        |     |
| Kommision  |     |
| Custommer  |     |
| Draft      |     |

### Objective

### Task list

- [] geometry
  - [] make / import native CAD files
  - [] create watertight surface and patches
  - [] triangulate surfaces
  - [] create stl files
  - [] create vtk files
- [] mesh
  - [] first mesh
  - [] mesh with good quality
  - [] boundary layers
  - [] mesh study
  - [] mesh refinement
- [] setup
  - [] set boundary conditions
  - [] define monitor points
- [] analysis
  - [] first post-analysis
  - [] define default-analysis
  - [] convergence behaviour
  - [] verification
  - [] final post-analysis
- [] geometry optimization
- [] write report

### Issues

## System

Short description of the hard- and hoftware used to create the analysis. The objective of this section is to ensure reproducebility of the results for the case of later reruns.

### Hardware

|          |                                                                                                                         |
| -------- | ----------------------------------------------------------------------------------------------------------------------- |
| Machine  | System: Dell Precision 7920 Tower <br> Model: 0RN4PJ v: A00                                                             |
| System   | Host: PC1620 <br> Kernel: 4.4.0-130-generic x86_64 (64 bit) <br> Distro: Ubuntu 16.04 xenial <br> Desktop: Gnome 3.18.5 |
| CPU      | 2 x Intel Xeon Platinum 8168s (-HT-MCP-SMP-) <br> Cache: 67584 KB <br> Max. clock speed: 3700 MHz                       |
| Memory   | Manufacturer: Hynix <br> Type: DDR4 <br>  Speed: 2666 MHz <br> Size: 12 x 32 GB                                         |
| Graphics | Card: NVIDIA Quadro P4000 <br> Display Server: X.Org 1.18.4 <br> Driver NVIDIA 384.130                                  |

### Software

| Task            | Programm                                         |
| --------------- | ------------------------------------------------ |
| CAD             | Solid Edge ST9                                   |
| stl             | Salome 8                                         |
| background mesh | OpenFoam 5.x                                     |
| mesh            | OpenFoam 5.x                                     |
| solver          | OpenFoam 5.x                                     |
| post processing | paraview 5.4.0 <br> R version 3.4.4 (2018-03-15) |

## CFD Setup

This section gives a brief overview of the properties and the boundary conditions used for the analysis. For deeper insight check the setup scripts.

### Analysis

| Characteristic  | Property               |
| --------------- | ---------------------- |
| time behavior   | transient/steady state |
| compressibility | in/compressible        |
| turbulence      | no                     |
| passive scalar  | yes                    |
| heat transfer   | no                     |
| buoyancy        | no                     |
| multiphase      | no                     |
| particles       | no                     |
| mesh motion     | no                     |
| large timesteps | yes                    |
| **solver**      | **pimpleFoam**         |

### Fluid properties

| Fluid            | dry air          |
| ---------------- | ---------------- |
| classification   | newtonian        |
| temperature $T$  | 20 $°C$          |
| pressure $p$     | 101325 $Pa$      |
| density $\rho$   | 1.189 $kg/m^3$   |
| viskosity  $\nu$ | 1.532e-5 $m^2/s$ |

Source:

>Verein Deutscher Ingenieure, and Gesellschaft Verfahrenstechnik und Chemieingenieurwesen. VDI-Wärmeatlas. Springer Vieweg, 2013; p.197

### Boundary conditions

| Boundary | U            | p            | s            |
| -------- | ------------ | ------------ | ------------ |
| inlet    | fixedValue   | zeroGradient | fixedValue   |
| outlet   | zeroGradient | fixedValue   | zeroGradient |
| wall     | zeroGradient | zeroGradient | zeroGradient |

### Usage